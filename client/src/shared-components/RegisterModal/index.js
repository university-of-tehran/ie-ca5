import React from "react";
import { Modal, Button, InputGroup, FormControl } from "react-bootstrap";

export default function RegisterModal(props) {
    return (
        <Modal show={props.show} onHide={props.handleClose}>
            <Modal.Header closeButton>
                <Modal.Title>ثبت نام</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <InputGroup className="mb-3">
                    <FormControl
                        placeholder="نام"
                        aria-label="fname"
                        aria-describedby="basic-addon1"
                    />
                </InputGroup>
                    <InputGroup className="mb-3">
                    <FormControl
                        placeholder="نام‌خانوادگی"
                        aria-label="lname"
                        aria-describedby="basic-addon1"
                    />
                </InputGroup>
                <InputGroup className="mb-3">
                    <FormControl
                        placeholder="ایمیل"
                        aria-label="email"
                        aria-describedby="basic-addon1"
                    />
                </InputGroup>
                <InputGroup className="mb-2">
                    <FormControl
                        placeholder="رمز عبور"
                        aria-label="password"
                        aria-describedby="basic-addon1"
                    />
                </InputGroup>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="primary">ورود</Button>
                <Button variant="outline-secondary" onClick={props.handleClose}>
                    انصراف
                </Button>
            </Modal.Footer>
        </Modal>
    );
}
