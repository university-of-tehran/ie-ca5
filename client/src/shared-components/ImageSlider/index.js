import React from "react";

import coverPhoto from "../../assets/cover-photo.jpg";

export default function ImageSlider() {
  return (
    <div class="img-slider">
      <img class="img-slider-item" src={coverPhoto} alt="cover" />
    </div>
  );
}
