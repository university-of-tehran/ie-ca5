import React from "react";
import { Modal, Button } from "react-bootstrap";

export default function ExitModal(props) {
    return (
        <Modal show={props.show} onHide={props.handleClose}>
            <Modal.Header closeButton>
                <Modal.Title>خروج از حساب کاربری</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <p>آیا می‌خواهید از حساب خود خارج شوید؟</p>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="danger" onClick={props.handleSignOut}>خروج</Button>
                <Button variant="outline-secondary" onClick={props.handleClose}>
                    انصراف
                </Button>
            </Modal.Footer>
        </Modal>
    );
}
