import React from "react";

export default function Footer() {
  return (
    <div class="footer">
      <div class="footer-right">
        <i class="flaticon-copyright"> </i>
        <div class="footer-text">دانشگاه تهران - سامانه جامع بلبل‌ستان</div>
      </div>
      <div class="social-icon-wrapper">
        <i class="flaticon-facebook"></i>
        <i class="flaticon-linkedin-logo"></i>
        <i class="flaticon-instagram"></i>
        <i class="flaticon-twitter-logo-on-black-background"></i>
      </div>
    </div>
  );
}
