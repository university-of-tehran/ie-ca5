import React, { Component } from "react";
import axios from "axios";
import ClipLoader from "react-spinners/ClipLoader";

import Header from "../../shared-components/Header";
import Footer from "../../shared-components/Footer";
import ImageSlider from "../../shared-components/ImageSlider";
import ProfileSidebar from "./components/ProfileSidebar";
import GradesSection from "./components/GradesSection";
import LoadingContext from "../../contexts/LoadingContext";

import "./style.css";

const spinnerStyle = {
    display: "block",
    margin: "100px auto",
    borderColor: "#1FA5FF",
};

export default class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            student: {
                name: "",
                secondName: "",
                id: "",
                birthDate: "",
            },
            gpa: null,
            totalPassedUnits: null,
            render: false,
            loading: false,
            setLoading: (loading) => {
                this.setState({ loading });
            },
        };
    }

    componentDidMount() {
        axios.get("http://localhost:8080/profile").then(
            (response) => {
                let data = response.data;
                this.setState({
                    student: data.student,
                    gpa: data.gpa,
                    totalPassedUnits: data.totalPassedUnits,
                    render: true,
                });
                this.state.setLoading(false)
            },
            (error) => {
                console.log(error);
                this.state.setLoading(false)
            }
        );
    }

    render() {
        const { student, gpa, totalPassedUnits, render } = this.state;
        return (
            <LoadingContext.Provider value={this.state}>
                <div
                    className="justify-content-start align-items-center"
                    style={{
                        minHeight: "100vh",
                        display: "flex",
                        flexDirection: "column",
                    }}
                >
                    <Header />
                    <ImageSlider />
                    <div className="middle">
                        <ProfileSidebar
                            student={student}
                            gpa={gpa}
                            totalPassedUnits={totalPassedUnits}
                            render={render}
                        />
                        <GradesSection gpa={gpa} render={render} />
                    </div>
                    <Footer />
                </div>
            </LoadingContext.Provider>
        );
    }
}
