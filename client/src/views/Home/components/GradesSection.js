import React, { Component } from "react";
import axios from "axios";
import { convertNumbersToPersian } from "../../../utils/perisan/normalizer";

export default class GradesSection extends Component {
    constructor(props) {
        super(props);
        this.state = {
            grades: [],
            courses: [],
            gpa: props.gpa,
            render: props.render,
        };

        this.findGradeCourse = this.findGradeCourse.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            gpa: nextProps.gpa,
            render: nextProps.render,
        });  
      }

    componentDidMount() {
        axios.get("http://localhost:8080/grades").then(
            (response) => {
                let data = response.data;
                this.setState({
                    grades: data.grades,
                    courses: data.courses,
                    render: true,
                });
            },
            (error) => {
                console.log(error);
            }
        );
    }

    findGradeCourse(grade) {
        for (const course of this.state.courses) {
            if (course.code == grade.code) {
                return course;
            }
        }
    }

    render() {
        const { grades, gpa } = this.state;

        if (!this.state.render) return null;

        return (
            <div className="middle-content">
                <div className="table-wrapper">
                    <div className="table-title">کارنامه</div>

                    {grades.map((grade, index) => {
                        let course = this.findGradeCourse(grade);
                        if (!course) return;

                        let gradeGood = grade.grade >= 10;

                        return (
                            <div className="table-row">
                                <div className="table-cell cell-number">
                                    {convertNumbersToPersian(index + 1)}
                                </div>
                                <div className="table-cell cell-code">
                                    {convertNumbersToPersian(grade.code)}
                                </div>
                                <div className="table-cell cell-name">
                                    {course.name}
                                </div>
                                <div className="table-cell cell-units">
                                    {convertNumbersToPersian(course.units)} واحد
                                </div>
                                <div className="table-cell cell-status">
                                    <div
                                        className={`status-value ${
                                            gradeGood
                                                ? "status-accept"
                                                : "status-reject"
                                        }`}
                                    >
                                        {gradeGood ? "قبول" : "مردود"}
                                    </div>
                                </div>
                                <div
                                    className={`table-cell cell-grade ${
                                        gradeGood ? "grade-good" : "grade-bad"
                                    }`}
                                >
                                    {convertNumbersToPersian(grade.grade)}
                                </div>
                            </div>
                        );
                    })}

                    <div className="table-footer">
                        <div className="table-average">
                            معدل: {convertNumbersToPersian(gpa)}
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}
