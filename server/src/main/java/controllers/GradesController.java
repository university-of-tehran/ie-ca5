package controllers;

import data.DataHandler;
import models.Grade;
import models.GradesPage;
import models.Profile;
import models.Student;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import server.Server;
import services.CoursesService;

import java.util.List;

@RestController
public class GradesController {

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/grades", method = RequestMethod.GET)
    public ResponseEntity getProfile() {
        if (Server.server.getLoggedInStudent() == null) {
            return new ResponseEntity<>("You're not logged in!", HttpStatus.UNAUTHORIZED);
        }
        Student student = Server.server.getLoggedInStudent();
        List<Grade> grades = DataHandler.getStudentGradesFromFile(student.id);

        GradesPage page = new GradesPage();
        page.student = student;
        page.grades = grades;
        page.courses = CoursesService.getCourses();
        return ResponseEntity.ok(page);
    }
}
