package controllers;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import server.Server;
import services.CoursesService;
import services.StudentsService;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.stream.Collectors;

@RestController
public class SubmitController {
    @CrossOrigin(origins = "*")
    @RequestMapping(value="/schedule/submit", method = RequestMethod.POST)
    public ResponseEntity submit (HttpServletRequest req) {
        if (Server.server.getLoggedInStudent() == null) {
            return new ResponseEntity<>("You're not logged in!", HttpStatus.UNAUTHORIZED);
        }

        String action = req.getParameter("action");
        if (action.equals("reset")) {
            Server.server.resetSelectedCourses();
            return ResponseEntity.ok("Reset successful!");
        } else if (action.equals("submit")) {
            String err = CoursesService.submit();
            if (!err.equals("")) {
                return new ResponseEntity<>("Submit failed!", HttpStatus.INTERNAL_SERVER_ERROR);
            }
            return ResponseEntity.ok("Submitted successfully!");
        }
        return new ResponseEntity<>("Unknown action!", HttpStatus.BAD_REQUEST);
    }
}
