package controllers;

import data.DataHandler;
import models.Grade;
import models.Profile;
import models.Student;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.http.ResponseEntity;
import server.Server;

import java.util.List;

@RestController
public class ProfileController {

    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/profile", method = RequestMethod.GET)
    public ResponseEntity getProfile() {
        if (Server.server.getLoggedInStudent() == null) {
            return new ResponseEntity<>("You're not logged in!", HttpStatus.UNAUTHORIZED);
        }
        Student student = Server.server.getLoggedInStudent();
        List<Grade> grades = DataHandler.getStudentGradesFromFile(student.id);
        Profile profile = new Profile();
        profile.student = student;
        profile.grades = grades;
        profile.gpa = Float.parseFloat(String.format("%,.2f", Server.server.getGpa()));
        profile.totalPassedUnits = Server.server.getTotalPassedUnits();
        return ResponseEntity.ok(profile);
    }
}
