package controllers;

import data.DataHandler;
import models.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import server.Server;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RestController
public class PlanController {
    @CrossOrigin(origins = "*")
    @RequestMapping(value = "/plan", method = RequestMethod.GET)
    public ResponseEntity getPlan() {
        if (Server.server.getLoggedInStudent() == null) {
            return new ResponseEntity<>("You're not logged in!", HttpStatus.UNAUTHORIZED);
        }

        List<Schedule> studentSchedules = DataHandler.getPlanScheduleForStudent(Server.server.getLoggedInStudent().id);
        List<Course> courses = DataHandler.getCoursesFromFile();
        String[][] tableEntries = new String[5][5];
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                tableEntries[i][j] = "";
            }
        }

        for (Schedule schedule: studentSchedules) {
            Course course = null;
            List<Integer> rows = new ArrayList<>();
            List<Integer> columns = new ArrayList<>();

            for (Course item : courses) {
                if (item.code.equals(schedule.code)) {
                    course = item;
                    break;
                }
            }

            assert course != null;
            if (Arrays.asList(course.classTime.days).contains("Saturday")) {
                rows.add(0);
            }
            if (Arrays.asList(course.classTime.days).contains("Sunday")) {
                rows.add(1);
            }
            if (Arrays.asList(course.classTime.days).contains("Monday")) {
                rows.add(2);
            }
            if (Arrays.asList(course.classTime.days).contains("Tuesday")) {
                rows.add(3);
            }
            if (Arrays.asList(course.classTime.days).contains("Wednesday")) {
                rows.add(4);
            }

            for (int row : rows) {
                if (course.classTime.time.equals("7:30-9:00")) {
                    tableEntries[row][0] = course.name;
                }
                if (course.classTime.time.equals("9:00-10:30")) {
                    tableEntries[row][1] = course.name;
                }
                if (course.classTime.time.equals("10:30-12:00")) {
                    tableEntries[row][2] = course.name;
                }
                if (course.classTime.time.equals("14:00-15:30")) {
                    tableEntries[row][3] = course.name;
                }
                if (course.classTime.time.equals("16:00-17:30")) {
                    tableEntries[row][4] = course.name;
                }
            }
        }
        return ResponseEntity.ok(tableEntries);
    }
}
