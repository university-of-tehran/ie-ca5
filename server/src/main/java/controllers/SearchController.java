package controllers;

import models.Course;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import server.Server;
import services.CoursesService;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

@RestController
public class SearchController {
    @CrossOrigin(origins = "*")
    @RequestMapping(value="/search", method = RequestMethod.GET)
    public ResponseEntity submit (HttpServletRequest req) {
        if (Server.server.getLoggedInStudent() == null) {
            return new ResponseEntity<>("You're not logged in!", HttpStatus.UNAUTHORIZED);
        }

        String action = req.getParameter("action");
        if (action.equals("search")) {
            String searchValue = req.getParameter("search");
            Server.server.setSearchFilter(searchValue);
            List<Course> allCourses = CoursesService.getCourses();
            List<Course> availableCourses = new ArrayList<>();
            String searchFilter = Server.server.getSearchFilter();
            for (Course c : allCourses) {
                if (c.name.toLowerCase().contains(searchFilter.toLowerCase())) {
                    availableCourses.add(c);
                }
            }
            return ResponseEntity.ok(availableCourses);
        } else if (action.equals("clear")) {
            Server.server.setSearchFilter("");
            return ResponseEntity.ok("Clear successful!");
        }

        return new ResponseEntity<>("Unknown action!", HttpStatus.BAD_REQUEST);
    }
}
