package controllers;

import data.DataHandler;
import models.Course;
import models.SchedulePage;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import server.Server;
import services.CoursesService;
import services.StudentsService;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@RestController
public class ScheduleController {

    @CrossOrigin(origins = "*")
    @RequestMapping(value="/schedule/delete", method = RequestMethod.DELETE)
    public ResponseEntity removeFromSchedule (HttpServletRequest req) {
        if (Server.server.getLoggedInStudent() == null) {
            return new ResponseEntity<>("You're not logged in!", HttpStatus.UNAUTHORIZED);
        }

        String courseCode = req.getParameter("course_code");
        String classCode = req.getParameter("class_code");

        Server.server.removeCourse(courseCode, classCode);
        return ResponseEntity.ok("Removed successfully!");
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value="/schedule", method = RequestMethod.GET)
    public ResponseEntity getSchedule (HttpServletRequest req) {
        if (Server.server.getLoggedInStudent() == null) {
            return new ResponseEntity<>("You're not logged in!", HttpStatus.UNAUTHORIZED);
        }

        SchedulePage page = new SchedulePage();
        page.selectedCourses = Server.server.selectedCourses;
        page.submitedCourses =  Server.server.lastSubmit;
        page.waitingCourses = Server.server.waitingCourses;

        return ResponseEntity.ok(page);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(value="/schedule/add", method = RequestMethod.POST)
    public ResponseEntity addToSchedule (HttpServletRequest req) {
        try {
            if (Server.server.getLoggedInStudent() == null) {
                return new ResponseEntity<>("You're not logged in!", HttpStatus.UNAUTHORIZED);
            }

            String requestData = req.getReader().lines().collect(Collectors.joining());
            JSONObject body = new JSONObject(requestData);
            String courseCode = body.getString("course_code");
            String classCode = body.getString("class_code");

            Course newCourse = null;

            List<Course> courses = DataHandler.getCoursesFromFile();
            for (Course c : courses) {
                if (c.code.equals(courseCode) && c.classCode.equals(classCode)) {
                    newCourse = c;
                }
            }
            if (newCourse == null) {
                return new ResponseEntity<>("No course with this info!", HttpStatus.NOT_FOUND);
            } else if (CoursesService.canBeAddedToSelectedCourses(newCourse)) {
                if (CoursesService.courseHaveCapacity(newCourse)) {
                    Server.server.addToSelectedCourses(newCourse);
                } else {
                    Server.server.addToWaitingCourses(newCourse);
                }
                return ResponseEntity.ok("Added to schedule!");
            } else {
                return new ResponseEntity<>("Couldn't add this course!", HttpStatus.INTERNAL_SERVER_ERROR);
            }
        } catch (JSONException | IOException e) {
            System.out.println(e.toString());
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
