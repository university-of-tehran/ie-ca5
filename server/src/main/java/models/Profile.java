package models;

import java.util.List;

public class Profile {
    public Student student;
    public List<Grade> grades;
    public float gpa;
    public int totalPassedUnits;
}
