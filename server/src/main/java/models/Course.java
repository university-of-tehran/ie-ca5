package models;

public class Course {
    public String code;
    public String classCode;
    public String name;
    public String type;
    public String Instructor;
    public int units;
    public ClassTime classTime;
    public ExamTime examTime;
    public int signedUp = 0;
    public int capacity;
    public String[] prerequisites;
}
