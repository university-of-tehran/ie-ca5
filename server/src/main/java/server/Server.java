package server;

import clients.BolbolestanClient;
import data.DataHandler;
import services.CoursesService;
import models.Course;
import models.Grade;
import models.Student;

import java.util.ArrayList;
import java.util.List;

public class Server {
    private Student loggedInStudent;
    private String searchFilter;
    public List<Course> selectedCourses;
    public List<Course> lastSubmit;
    public List<Course> waitingCourses;
    private int totalPassedUnits;
    private float gpa;

    public static Server server;
    static {
        server = new Server();
    }

    public Server() {
        BolbolestanClient.loadData();
        this.loggedInStudent = null;
        this.searchFilter = "";
        this.selectedCourses = new ArrayList<>();
        this.lastSubmit = new ArrayList<>();
        this.waitingCourses = new ArrayList<>();
        this.totalPassedUnits = 0;
        this.gpa = 0;
    }

    public void logInStudent(Student s) {
        this.loggedInStudent = s;
        this.searchFilter = "";
        this.selectedCourses = CoursesService.loadSubmittedCourses(s.id);
        this.lastSubmit = CoursesService.loadSubmittedCourses(s.id);
        this.calcTotalPassedUnitsAndGpa();
    }

    public void logOutStudent() {
        this.loggedInStudent = null;
        this.searchFilter = "";
        this.selectedCourses = new ArrayList<>();
        this.lastSubmit = new ArrayList<>();
        this.totalPassedUnits = 0;
        this.gpa = 0;
    }

    public Student getLoggedInStudent() {
        return this.loggedInStudent;
    }

    public void setSearchFilter(String s) {
        this.searchFilter = s;
    }

    public String getSearchFilter() {
        return this.searchFilter;
    }

    public List<Course> getSelectedCourses() {
        return this.selectedCourses;
    }

    public List<Course> getLastSubmit() {
        this.resetLastSubmit();
        return this.lastSubmit;
    }

    public void addToSelectedCourses(Course course) {
        this.selectedCourses.add(course);
    }

    public void removeCourse(String courseCode, String classCode) {
        this.selectedCourses.removeIf(c -> c.code.equals(courseCode) && c.classCode.equals(classCode));
    }

    public void resetSelectedCourses() {
        this.selectedCourses = CoursesService.loadSubmittedCourses(this.loggedInStudent.id);
    }

    public void resetLastSubmit() {
        this.lastSubmit = CoursesService.loadSubmittedCourses(this.loggedInStudent.id);
    }

    public void calcTotalPassedUnitsAndGpa() {
        List<Grade> grades = DataHandler.getStudentGradesFromFile(this.loggedInStudent.id);
        List<Course> courses = DataHandler.getCoursesFromFile();

        int totalPassedUnits = 0;
        float sum = 0;
        for (Grade grade: grades) {
            for (Course course: courses) {
                if (grade.code.equals(course.code)) {
                    if (grade.grade > 10) {
                        totalPassedUnits += course.units;
                        sum += course.units * grade.grade;
                    }
                }
            }
        }

        this.totalPassedUnits = totalPassedUnits;
        this.gpa = sum / totalPassedUnits;
    }

    public float getGpa() {
        return this.gpa;
    }

    public int getTotalPassedUnits() {
        return this.totalPassedUnits;
    }

    public List<Course> getWaitingCourses() {
        return this.waitingCourses;
    }

    public void addToWaitingCourses(Course course) {
        for (Course c: this.lastSubmit) {
            if (c.code == course.code) {
                return;
            }
        }

        this.waitingCourses.add(course);
    }

    public void submitWaitingCourses() {
        for (Course course: this.waitingCourses) {
            this.lastSubmit.add(course);
            this.selectedCourses.add(course);
        }

        this.waitingCourses = new ArrayList<>();
    }
}
