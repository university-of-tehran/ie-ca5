package clients;

public class Constants {
    public static final String GET_AVAILABLE_COURSES = "http://138.197.181.131:5000/api/courses";
    public static final String GET_STUDENTS = "http://138.197.181.131:5000/api/students";
    public static final String GET_STUDENT_GRADES = "http://138.197.181.131:5000/api/grades/";
}
