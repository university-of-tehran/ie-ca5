package clients;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import models.Grade;
import models.Student;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import data.DataHandler;

import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static data.DataHandler.*;


public class BolbolestanClient {
    public static void loadData() {
        BolbolestanClient.getCourses();
        BolbolestanClient.getStudents();
        BolbolestanClient.getGrades();
    }

    public static void getCourses() {
        Document doc = null;
        try {
            doc = Jsoup.connect(Constants.GET_AVAILABLE_COURSES).ignoreContentType(true).get();
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println(e.toString());
        }

        assert doc != null;
        writeCoursesDocToFile(doc);
    }

    public static void getStudents() {
        Document doc = null;
        try {
            doc = Jsoup.connect(Constants.GET_STUDENTS).ignoreContentType(true).get();
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println(e.toString());
        }
        assert doc != null;
        writeStudentsDocToFile(doc);
    }

    public static void getGrades() {
        List<Student> students = DataHandler.getStudentsFromFile();

        Map<String, List<Grade>> studentIdToGradeMapping = new HashMap<>();
        for(Student student: students) {
            studentIdToGradeMapping.put(student.id, BolbolestanClient.getGradesForStudent(student.id));
        }

        writeGradesMapToFile(studentIdToGradeMapping);
    }

    public static List<Grade> getGradesForStudent(String studentId) {
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();

        Document doc = null;

        try {
            doc = Jsoup.connect(Constants.GET_STUDENT_GRADES + studentId).ignoreContentType(true).get();
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println(e.toString());
        }

        Type gradeListType = new TypeToken<ArrayList<Grade>>(){}.getType();
        assert doc != null;
        return gson.fromJson(doc.text(), gradeListType);
    }

}
