package services;

import data.DataHandler;
import models.Student;
import server.Server;

import java.util.List;

public class StudentsService {
    public static boolean loginIfExists(String studentId) {
        List<Student> students = DataHandler.getStudentsFromFile();
        for (Student s: students) {
            if (s.id.equals(studentId)) {
                Server.server.logInStudent(s);
                return true;
            }
        }
        return false;
    }

    public static void logout() {
        Server.server.logOutStudent();
    }
}
