package data;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import models.Course;
import models.Grade;
import models.Schedule;
import models.Student;
import org.jsoup.nodes.Document;

import java.io.*;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static data.Constants.*;

public class DataHandler {
    public static List<Course> getCoursesFromFile() {
        List<Course> courses = new ArrayList<>();
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();

        try {
            BufferedReader reader = new BufferedReader(new FileReader(COURSES_FILE_PATH));
            Type courseListType = new TypeToken<ArrayList<Course>>(){}.getType();
            courses = gson.fromJson(reader, courseListType);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return courses;
    }

    public static List<Student> getStudentsFromFile() {
        List<Student> students = new ArrayList<>();
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();

        try {
            BufferedReader reader = new BufferedReader(new FileReader(STUDENTS_FILE_PATH));
            Type studentListType = new TypeToken<ArrayList<Student>>(){}.getType();
            students = gson.fromJson(reader, studentListType);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return students;
    }

    public static List<Grade> getStudentGradesFromFile(String studentId) {
        Map<String, List<Grade>> studentIdToGradeMapping = new HashMap<>();
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();

        try {
            BufferedReader reader = new BufferedReader(new FileReader(GRADES_FILE_PATH));
            Type gradesListType = new TypeToken<Map<String, List<Grade>>>(){}.getType();
            studentIdToGradeMapping = gson.fromJson(reader, gradesListType);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return studentIdToGradeMapping.getOrDefault(studentId, null);
    }

    public static List<Schedule> getPlanScheduleForStudent(String studentId) {
        List<Schedule> allSchedules = new ArrayList<>();
        List<Schedule> studentSchedules = new ArrayList<>();
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();

        try {
            BufferedReader reader = new BufferedReader(new FileReader(SCHEDULES_FILE_PATH));
            Type schedulesListType = new TypeToken<ArrayList<Schedule>>(){}.getType();
            allSchedules = gson.fromJson(reader, schedulesListType);
        } catch (IOException e) {
            e.printStackTrace();
        }

        for (Schedule item: allSchedules) {
            if (item.StudentId.equals(studentId)) {
                studentSchedules.add(item);
            }
        }

        return studentSchedules;
    }

    public static List<Schedule> getAlSchedulesFromFile() {
        List<Schedule> schedules = new ArrayList<>();
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();

        try {
            BufferedReader reader = new BufferedReader(new FileReader(SCHEDULES_FILE_PATH));
            Type scheduleListType = new TypeToken<ArrayList<Schedule>>(){}.getType();
            schedules = gson.fromJson(reader, scheduleListType);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return schedules;
    }

    public static void writeCoursesDocToFile(Document doc) {
        File coursesFile = new File(COURSES_FILE_PATH);
        try {
            FileWriter writer = new FileWriter(coursesFile);
            writer.write(doc.text());
            writer.close();
        } catch (IOException e) {
            System.out.println("An error occurred during writing courses to file.");
            e.printStackTrace();
            System.out.println(e.toString());
        }
    }

    public static void writeStudentsDocToFile(Document doc) {
        File studentsFile = new File(STUDENTS_FILE_PATH);
        try {
            FileWriter writer = new FileWriter(studentsFile);
            writer.write(doc.text());
            writer.close();
        } catch (IOException e) {
            System.out.println("An error occurred during writing students to file.");
            e.printStackTrace();
            System.out.println(e.toString());
        }
    }

    public static void writeGradesMapToFile(Map<String, List<Grade>> studentIdToGradeMapping) {
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        File gradesFile = new File(GRADES_FILE_PATH);
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(gradesFile));
            gson.toJson(studentIdToGradeMapping, writer);
            writer.flush();
            writer.close();
        } catch (IOException e) {
            System.out.println("An error occurred during writing grades to file.");
            e.printStackTrace();
            System.out.println(e.toString());
        }
    }

    public static void writeSchedulesListToFile(List<Schedule> schedules) {
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();

        File schedulesFile = new File(SCHEDULES_FILE_PATH);
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(schedulesFile));
            gson.toJson(schedules, writer);
            writer.flush();
            writer.close();
        } catch (IOException e) {
            System.out.println("An error occurred during writing schedules to file.");
            e.printStackTrace();
            System.out.println(e.toString());
        }
    }

    public static void writeCoursesListToFile(List<Course> courses) {
        File coursessFile = new File(COURSES_FILE_PATH);
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();

        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(coursessFile));
            gson.toJson(courses, writer);
            writer.flush();
            writer.close();
        } catch (IOException e) {
            System.out.println("An error occurred during writing students to file.");
            e.printStackTrace();
            System.out.println(e.toString());
        }
    }
}