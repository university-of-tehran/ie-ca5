package data;

public class Constants {
    public static final String COURSES_FILE_PATH = "./src/main/java/data/courses.json";
    public static final String STUDENTS_FILE_PATH = "./src/main/java/data/students.json";
    public static final String GRADES_FILE_PATH = "./src/main/java/data/grades.json";
    public static final String SCHEDULES_FILE_PATH = "./src/main/java/data/schedules.json";
}
